#!/usr/bin/env bash
set -euo pipefail

main() {
  must_set GOOGLE_PROJECT
  must_set GOOGLE_ZONE
  must_set GITALY_SHARD_GOOGLE_INSTANCE_NAME
  must_set DELETION_OCCURRED_AFTER
  must_set HASHED_PATHS
  must_set USER
  must_set KEY_FILE

  snapshot=$(gcloud --project "${GOOGLE_PROJECT}" compute snapshots list --format 'table[no-heading](name)' \
    --sort-by '~creationTimestamp' \
    --filter "creationTimestamp<${DELETION_OCCURRED_AFTER} AND sourceDisk ~ ${GOOGLE_ZONE}/disks/${GITALY_SHARD_GOOGLE_INSTANCE_NAME}-data" \
    --limit 1)

  disk_name="${GITALY_SHARD_GOOGLE_INSTANCE_NAME}-snapshot-restore"

  disk_created=$(gcloud compute disks list --project "${GOOGLE_PROJECT}" --filter="name:${disk_name}")

  if [ "${disk_created}" = "" ]; then
    echo "creating disk from snapshot ${snapshot}"
    gcloud --project "${GOOGLE_PROJECT}" compute disks create "${disk_name}" \
      --zone "${GOOGLE_ZONE}" --source-snapshot "${snapshot}"
  fi

  instance_desc=$(gcloud compute instances describe "${GITALY_SHARD_GOOGLE_INSTANCE_NAME}" \
    --project "${GOOGLE_PROJECT}" --zone "${GOOGLE_ZONE}")

  if ! echo "${instance_desc}" | grep "${disk_name}" >/dev/null 2>&1; then
    echo "attaching disk ${disk_name} to instance ${GITALY_SHARD_GOOGLE_INSTANCE_NAME}"
    gcloud --project "${GOOGLE_PROJECT}" compute instances attach-disk \
      "${GITALY_SHARD_GOOGLE_INSTANCE_NAME}" --disk "${disk_name}" \
      --device-name gitaly-restore --zone "${GOOGLE_ZONE}"
  fi

  echo "mounting recovery disk"
  on_gitaly_shard "if ! mountpoint -q /mnt ; then sudo mount /dev/disk/by-id/google-gitaly-restore /mnt; fi"

  # shellcheck disable=SC2206
  hashed_paths_array=(${HASHED_PATHS//,/ })

  for idx in "${!hashed_paths_array[@]}"; do
    hashed_path=${hashed_paths_array[$idx]}
    echo "restoring hashed path ${hashed_path}"
    backup_path_base="/mnt/git-data/repositories/${hashed_path}"
    target_path_base="/var/opt/gitlab/git-data/repositories/${hashed_path}"

    for repo in git wiki.git design.git; do
      backup_path="${backup_path_base}.${repo}"
      target_path="${target_path_base}.${repo}"

      on_gitaly_shard "$(declare -f assert_sane_target); assert_sane_target ${target_path}"

      repo_parent="$(dirname "${target_path}")"
      repo_grandparent="$(dirname "${repo_parent}")"
      on_gitaly_shard "$(declare -f create_gitaly_hash_subdir); create_gitaly_hash_subdir ${repo_grandparent}"
      on_gitaly_shard "$(declare -f create_gitaly_hash_subdir); create_gitaly_hash_subdir ${repo_parent}"

      echo "restoring ${target_path}"
      on_gitaly_shard "$(declare -f cp_target); cp_target ${backup_path} ${target_path}"
    done
  done

  echo "unmounting recovery disk"
  on_gitaly_shard "sudo umount /mnt"

  echo "detaching recovery disk"
  gcloud --project "${GOOGLE_PROJECT}" compute instances detach-disk \
    "${GITALY_SHARD_GOOGLE_INSTANCE_NAME}" \
    --device-name gitaly-restore --zone "${GOOGLE_ZONE}"

  echo "deleting recovery disk"
  gcloud --quiet --project "${GOOGLE_PROJECT}" compute disks delete "${disk_name}" --zone "${GOOGLE_ZONE}"
}

on_gitaly_shard() {
  gcloud --project "${GOOGLE_PROJECT}" compute ssh "${USER}@${GITALY_SHARD_GOOGLE_INSTANCE_NAME}" \
    --internal-ip --ssh-key-file="${KEY_FILE}" --zone "${GOOGLE_ZONE}" --command "${1}"
}

assert_sane_target() {
  target_path=$1

  if sudo test -e "${target_path}"; then
    echo "!!! ${target_path} already exists - bailing!"
    return 0
  fi
}

cp_target() {
  backup_path=$1
  target_path=$2
  if sudo test -d "${backup_path}"; then
    sudo cp -a "${backup_path}" "${target_path}"
    return 0
  fi
  echo "!!! ${backup_path} does not exist!"
}

create_gitaly_hash_subdir() {
  if sudo test -d "$1"; then
    echo "$1 already exists"
    return 0
  fi

  echo "$1 does not exist, creating it with gitaly ownership and mode"
  sudo mkdir "$1"
  sudo chown git:root "$1"
  sudo chmod 2750 "$1"
}

must_set() {
  if [ -z "${!1:-}" ]; then
    echo "must set $1"
    return 1
  fi
}

main
