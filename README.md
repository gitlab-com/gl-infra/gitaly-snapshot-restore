# gitaly-snapshot-restore

## What?

A script that restores one or more git repositories to a single Gitaly shard.

## Why?

If we are restoring many accidentally-deleted repositories at once (say, because
a group was recursively deleted)

## How?

Prerequisites:

1. The GitLab instance using the Gitaly shard in question contains project
   metadata for the lost repositories - they are just not present on disk.
   - Note that the web UI will return 404 when attempting to view the project
     pages of such repositories.
   - https://gitlab.com/gitlab-org/gitlab/-/issues/241379 will allow us to
     import artifacts that restore deeply-nested group+project hierarchies.
1. A GCE disk snapshot of the Gitaly shard's data disk taken before the deletion
   occurred exists.
1. A runner with no external IP that is in the same subnet as the gitaly shards.
1. A user and a key that already exists in the gitaly shard to be used with SSH.
1. IAP tunneling must be enabled on the gitaly shard via a network tag `iap-tunnel`

Set the following environment variables:

- `GOOGLE_PROJECT`
- `GOOGLE_ZONE`
  instance from the GCP console.
- `DELETION_OCCURRED_AFTER`: An RFC3339 timestamp, e.g. "2020-08-27T10:00:00Z"
- `HASHED_PATHS`: A comma-separated list of hashed paths to restore. e.g.
  "@hashed/ab/cd/abcd,@hashed/12/34/1234"
- `GITALY_SHARD_GOOGLE_INSTANCE_NAME`: Not the FQDN, but the name of the
- `KEY` and `PUBLIC_KEY`: An SSH key to be used by the runner to connect to the gitaly shards.
- `USER`: The user used for SSH.

Ensure that the local gcloud context can access the relevant resources. Either
ensure [Application Default Credentials](https://developers.google.com/identity/protocols/application-default-credentials)
are in the expected location and run `gcloud auth application-default`, or
activate a service account with `gcloud auth activate-service-account`.

Run the script: `./gitaly-snapshot-restore.sh`.

If running in a CI pipeline, ensure GOOGLE_SERVICE_ACCOUNT_KEY is configured as
a secret CI var.

The service account must have the following permissions:
  - Compute Instance Admin (v1)
  - Service Account User
  - IAP-secured Tunnel User

## Development and further work

The script currently only cleans up after itself on a successful run. The script
might fail if `gcloud compute ssh` fails. Fixing the issue and re-running should
unmount and remove the disk.

The script can be rewritten to exclude the `gcloud compute ssh` command and use SSH directly.

A more permanent user and key could be added to chef to specifically SSH into gitaly nodes.
