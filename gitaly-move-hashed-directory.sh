#!/usr/bin/env bash
set -euo pipefail

main() {
  must_set KEY_FILE
  must_set NEW_HASHED_PATH
  must_set GITALY_SHARD
  must_set GOOGLE_PROJECT
  must_set GOOGLE_ZONE
  must_set ORIGINAL_HASHED_PATH
  must_set USER

  for repo in git wiki.git design.git; do
    original_path="${ORIGINAL_HASHED_PATH}.${repo}"
    new_path="${NEW_HASHED_PATH}.${repo}"

    on_gitaly_shard "$(declare -f assert_new_repo_does_not_exist); assert_new_repo_does_not_exist ${original_path}"

    repo_parent="$(dirname "${new_path}")"
    repo_grandparent="$(dirname "${repo_parent}")"
    on_gitaly_shard "$(declare -f create_gitaly_hash_subdir); create_gitaly_hash_subdir ${repo_grandparent}"
    on_gitaly_shard "$(declare -f create_gitaly_hash_subdir); create_gitaly_hash_subdir ${repo_parent}"

    echo "moving ${original_path} to ${new_path}"
    on_gitaly_shard "$(declare -f mv_target); mv_target ${original_path} ${new_path}"
  done

  on_gitaly_shard() {
    gcloud --project "${GOOGLE_PROJECT}" compute ssh "${USER}@${GITALY_SHARD}" \
      --internal-ip --ssh-key-file="${KEY_FILE}" --zone "${GOOGLE_ZONE}" --command "${1}"
  }

  assert_new_repo_does_not_exist() {
    new_path=$1

    if sudo test -e "${new_path}"; then
      echo "!!! ${new_path} exists!"
      return 0
    fi
  }

  mv_target() {
    original_path=$1
    new_path=$2
    if sudo test -d "${original_path}"; then
      sudo mv -v "${original_path}" "${new_path}"
      return 0
    fi
    echo "!!! ${original_path} does not exist!"
  }

  create_gitaly_hash_subdir() {
    if sudo test -d "$1"; then
      echo "$1 already exists"
      return 0
    fi

    echo "$1 does not exist, creating it with gitaly ownership and mode"
    sudo mkdir "$1"
    sudo chown git:root "$1"
    sudo chmod 2750 "$1"
  }

  must_set() {
    if [ -z "${!1:-}" ]; then
      echo "must set $1"
      return 1
    fi
  }
}

main
